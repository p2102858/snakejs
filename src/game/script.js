const canvas = document.querySelector('.game');

const canvas_width = parseInt(canvas.getAttribute("width"));
const canvas_height = parseInt(canvas.getAttribute("height"));

const ctx = canvas.getContext("2d");
const canvasBackground = ["#4fa834","#48ff00"];

const tile_size = canvas_width / 20;

const UP = 0;
const RIGHT = 1;
const DOWN = 2;
const LEFT = 3;

let delay = 200;

let facing;

const snakeCoordinates = [{'x' : 9, 'y':9}, {'x' : 10, 'y' : 9}, {'x' : 11,'y' : 9}, {'x' : 12,'y' : 9}];
let snake = [];

let mapSize;

let map = [];

let running = true;

let fruit;

let score = 0;

document.addEventListener("keydown",(event)=>{
    switch(event.key){
        case('d'): 
        case('D'):
        case("ArrowRight"):
            if (facing !== LEFT){
                facing = RIGHT; 
            }
            break;
        case('s') : 
        case('S'):
        case("ArrowDown"):
            if(facing !== UP){
                facing = DOWN; 
            }
            break;
        case('q'): 
        case('Q'):
        case('a'):
        case('A'):
        case("ArrowLeft"):
            if (facing !== RIGHT){
                facing = LEFT; 
            }    
            break;
        case("z"):
        case('Z'):
        case('w'):
        case('W'):
        case("ArrowUp"):
            if(facing !== DOWN){
                facing=UP; 
            }
            break;
    }
})

class Tile{
    // Tuile vide
    x = 0;
    y = 0;

    constructor(x,y){
        this.x = x;
        this.y = y;
    }

    isFull(){
        return false;
   }

    isSolid(){
        return false;            
    }

    isFruit(){
        return false;
    }
}

class Fruit extends Tile{
    color = "yellow";

    constructor(){
        fruit = getRandomCoordinates();
        while(map[fruit.y][fruit.x].isFull()) fruit = getRandomCoordinates();
        super(fruit.x,fruit.y);
        map[this.y][this.x] = this;
    }

    isFull(){
        return true;
    }

    isFruit(){
        return true;
    }
}

class Wall extends Tile{
    // Case de mur

    constructor(x,y){
        super(x,y);
    }

    isFull(){
        return true;
    }

    isSolid(){
        return true;
    }

    draw(){
        ctx.fillStyle = this.color;
        ctx.strokeStyle = this.strokeColor;
        ctx.fillRect(this.x * tile_size, this.y * tile_size, tile_size, tile_size);
        ctx.strokeRect(this.x * tile_size, this.y * tile_size, tile_size, tile_size);
    }
}

class Head extends Wall{
    // Tete du snake
    child;

    oldX;
    oldY;

    color = "red";

    id = 0;

    constructor(x,y){
        super(x,y);
    }

    placeOnMap(){
        map[this.y][this.x] = this;
    }

    detectCollision(){
        if(map[this.y][this.x] !== this && map[this.y][this.x].isSolid()){
            console.log(map[this.y][this.x])
            gameOver();}
        if(map[this.y][this.x].isFruit()){ 
            new Fruit();
            this.grow();
        }
    }
    
    forward(){
        console.log("head forward")
        // Ancienne cooronnées, elles seront assignees a la tuile suivante
        this.oldY = this.y;
        this.oldX = this.x;
        // Mise à jour des coordonnées en fonction de la direction
        switch(facing){
            case(UP):
                this.y--;
                break;
            case(LEFT):
                this.x--;
                break;
            case(DOWN):
                this.y++;
                break;
            case(RIGHT):
                this.x++;
                break;
            default:
                break;
        }
        // if(this.x < 0 || this.y < 0 || this.x > canvas_width / tile_size || this.y > canvas_width / tile_size) gameOver()
        this.detectCollision();
        // La tuile avance
        map[this.y][this.x] = this;
        // Mise a jour du tableau snake
        snake[this.id] = {x:this.x, y:this.y};
        // Appel de la methode forward du suivant
        this.child.forward();
    }

    isSolid(){
        return true;
    }

    grow(){
        Tail.willGrow = true;
    }

    draw(){
        ctx.fillStyle = this.color;
        ctx.strokeStyle = this.strokeColor;
        ctx.fillRect(this.x * tile_size, this.y * tile_size, tile_size, tile_size);
        ctx.strokeRect(this.x * tile_size, this.y * tile_size, tile_size, tile_size);
    }
}

class Tail extends Head{
    // Partie du corps du snake
    parent;

    static willGrow = false;

    color="black";
    strokeColor = "rgba(0,0,0,0)";
    id;

    constructor(x,y,parent,id){
        super(x,y);
        this.parent = parent;
        this.parent.child=this;
        this.id = id;
    }

    isSolid(){
        return true;
    }
    
    forward(){
        console.log("tail "+this.id+ " forward")
        this.oldX = this.x;
        this.oldY = this.y;
        // On recupere les anciennes coordonnees du suivant
        this.x = this.parent.oldX;
        this.y = this.parent.oldY;

        // La tuile avance
        map[this.y][this.x] = this;
        // Mise a jour du tableau snake
        snake[this.id] = {x:this.x, y:this.y};

        if(!Tail.willGrow && this.child == undefined){
            // si la tuile actuelle est la derniere du snake, on remplace son ancienne position par une tuile vide
            map[this.oldY][this.oldX] = new Tile(this.oldX,this.oldY); 

        }else if (Tail.willGrow && this.child==undefined){
            this.grow();
        }
        else{
            // sinon, on appelle la methode forward du suivant
            this.child.forward();
        }
        
    }

    grow(){
        let newTail = new Tail(this.oldX,this.oldY,this,this.id+1);
        map[this.oldY][this.oldX]=newTail;
        snake[snake.length] = newTail;
        Tail.willGrow = false;
    }

    draw(){
        ctx.fillStyle = this.color;
        ctx.strokeStyle = this.strokeColor;
        ctx.fillRect(this.x * tile_size, this.y * tile_size, tile_size, tile_size);
        ctx.strokeRect(this.x * tile_size, this.y * tile_size, tile_size, tile_size);
    }
}

function drawMap(){
    let ii = 0;
    for(let i = 0; i < canvas_width / tile_size; i++){
        for(let j = 0; j < canvas_width / tile_size; j++){
            ctx.fillStyle = canvasBackground[ii%2];
            ctx.fillRect(i*tile_size,j*tile_size,tile_size,tile_size);
            ii++;
        }
        if(ii % 2 === 0) ii = 1;
        else ii = 0;
    }
    drawFruit();
    drawSnake();
}

function drawFruit(){
    ctx.fillStyle = "yellow";
    ctx.fillRect(fruit.x*tile_size,fruit.y*tile_size,tile_size,tile_size);
}

function drawSnake(){
    for(let part of snake){
        if(part == snake[0]) ctx.fillStyle = "red";
        else ctx.fillStyle = "black";
        ctx.fillRect(part.x*tile_size,part.y*tile_size,tile_size,tile_size);
    }
    // pause()
}

function tileIsInSnake(x,y){
    for(let part of snake){
        if(part.x === x ||part.y === y) return true;
    }
    return false;
}

function getRandomCoordinates(){
    randX = Math.floor(Math.random()*canvas_height/tile_size);
    randY = Math.floor(Math.random()*canvas_height/tile_size);
    if(tileIsInSnake(randX,randY))getRandomCoordinates();
    return {x:randX,y:randY}
}

function fillMap(){
    map = Array(canvas_height / tile_size);
    for(let i = 0; i < canvas_height/tile_size; i++){
        let line = Array(canvas_width / tile_size);
        for(let j = 0; j < canvas_width / tile_size; j++){
            line[j] = new Tile(j,i);            
        }
        map[i] = line;
    } 
    console.log(map);
    // createSnake();
}

function createSnake(){
    let previousPart;
    let i = 0;
    for(let part of snake){
        if(part === snake[0]){ 
            previousPart = new Head(part.x,part.y);
            map[part.y][part.x] = previousPart;
        }
        else{
            previousPart = new Tail(part.x,part.y,previousPart,i);
            map[part.y][part.x] = previousPart;
        }
        i++;
    }
}

function initSnake(){
    snake = Array(4);
    let i = 0;
    let lastTile;
    //[{'x' : 9, 'y':9}, {'x' : 10, 'y' : 9}, {'x' : 11,'y' : 9}, {'x' : 12,'y' : 9}]
    for(let part of snakeCoordinates){
        if(i === 0){
            lastTile = new Head(part.x,part.y);
            snake[i] = lastTile;
            lastTile.placeOnMap();
        }
        else{
            lastTile = new Tail(part.x,part.y,lastTile,i);
            snake[i] = lastTile;
            lastTile.placeOnMap();
        }
        i++;
    }
    console.log(snake);
    facing = LEFT;
}


function gameOver(){ 
    window.location.href="./game.html";
    running = false;
}

function init(){
    fillMap(); 
    initSnake();
    facing = LEFT;
    score = 0;
    fillMap();
    new Fruit();
    running = true;
}

function tick() {
    return new Promise(resolve => setTimeout(resolve, delay));
}

async function run(){
    drawMap();
    let head = snake[0];
    try{
        while(running){
            head.forward();
            drawMap();
            await tick();
        }
    }catch(TypeError){
        console.log("type error")
        gameOver();
    }
}

async function pause(){
    running = !running;
}
function main(){
    init();
    run();
}
main();